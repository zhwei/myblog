Title: python 线程 “终止”的一个解决方案
Date: 2013-11-09 09:22
Tags: python,thread
Category: Python
Slug: one-way-stop-thread-in-python
Author: zhwei
Summary: 之所以加引号是因为这个方法不是真正的终止， 并不是kill a thread，因为直接终止一个线程会导致一些问题，不如这个线程执行的时候占用的资源会一直阻塞下去， 这里说的是破坏线程赖以执行的条件。

 之所以加引号是因为这个方法不是真正的终止， 并不是kill a thread，因为直接终止一个线程会导致一些问题，不如这个线程执行的时候占用的资源会一直阻塞下去， 这里说的是破坏线程赖以执行的条件。


比如
```python

class RpcServerThread(threading.Thread):
    """ SimpleXMLRPCServer
    func run and stop
    """
    running=True
    def run(self):
        self.server = SimpleXMLRPCServer.SimpleXMLRPCServer((IP, PORT))
        obj = WmiObj()
        self.server.register_instance(obj)
        self._set_daemon()
        while self.running:
            self.server.handle_request()

    def stop(self):
        self.running = False
        self.server.server_close()
```

这里线程中包含一个循环，如果条件一直成立那就一直执行下去，stop方法就是改变了线程赖以判断循环的条件，然后线程跳出循环，自然终止。
