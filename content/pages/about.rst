About
###################



zhwei
----------------

Nick Name: zhwei

真实姓名： 张卫

格言
----------------

做最好的自己！

简介
---------------

杏园中的野草。

真诚的Pythoner， 热爱生活的程序猿。

有代码洁癖，git深度患者。


**Github Résumé**: http://resume.github.io/?zhwei

**OSC Git**: http://git.oschina.net/zhwei


作品
---------------

.. list-table::   
  :widths: 10 15 30
  :header-rows: 1

  * - 名称
    - 用了什么
    - 简介
  * - `Gotit <https://github.com/zhwei/gotit>`_
    - urllib, web.py, redis,
    - 校内信息查询平台
  * - `汽配商城 <http://git.oschina.net/zhwei/qipei>`_
    - Django
    - 多商家多用户在线商城
  * - `ServerMonitor <http://git.oschina.net/zhwei/ServerMonitor>`_
    - Bottle, mongodb,pycurl, wmi, xmlrpclib
    - 服务器&网站在线监控平台

小东西
~~~~~~~~~~~~~~~~

+ 校内视频在线版 https://github.com/zhwei/sdut-cumpus-video
+ 静态网站加速 https://gist.github.com/zhwei/7429973



Nice Day!

*update at: 2013-11-12*.
