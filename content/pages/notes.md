Title: Notes
Slug: notes

_乱而无序的笔记_

+ git查看远端分支`git branch -a`, git删除远端分支`git push origin --delete <branch name>`, git删除本地分支`git branch -d <branch name>`

+ 当git使用`https`clone时,之后的每次`pull`和`push`都需要提供用户名和密码,而是用ssh clone的则不需要, 并且ssh方式要比https易于验证, 使用https遇到过长时间不能验证的情况

+ django添加用户到组需要 `dian = Group.objects.filter(name='dian')[0]  user.groups.add(dian)`

+ `Nginx Error - 413 Request Entity Too Large`在`server`中添加`client_max_body_size 20M;`[link](http://stackoverflow.com/questions/5001092/413-request-entity-too-large-the-web-server-connection-was-closed-error-64)

+ `git pull`是出现`error: insufficient permission for adding an object to repository database .git/objects`, 见[http://stackoverflow.com/questions/6448242/git-push-error-insufficient-permission-for-adding-an-object-to-repository-datab](http://stackoverflow.com/questions/6448242/git-push-error-insufficient-permission-for-adding-an-object-to-repository-datab)

+ `sudo: unable to resolve host ... ` 在`/etc/hosts`文件中添加主机名到`127.0.0.1`的映射

+ 编译安装`nginx`, `./configure --prefix=/opt/nginx --with-http_gzip_static_module --with-http_flv_module --with-http_ssl_module`, `make`

+ 缺少`python.h`安装`python-dev`

+ django 使用 tinymce1.5.1, 同步生成数据库时, 会出现`ImportError: cannot import name smart_unicode`报错, 在其官方[issue](https://code.google.com/p/django-tinymce/issues/detail?id=63) 已有提出, 可以手动将`...site-packages/tinymce/widgets.py`文件第14行改成 `from django.utils.encoding import smart_unicode`

+ django 使用 image model 时需要安装 PIL ,在 virtualenv 使用时需要再次安装, apt 安装的python-imaging 并不起作用, 可以在 [link](http://www.pythonware.com/products/pil/) 下载到包, 已经下载并保存到dropbox.

+ django自定义表单工具 autoforms

+ sublime text 2 中的 `zencoding`现在叫做 `emmet`

+ coffee 中 `@` = `this`
+ ubuntu 中virtualbox安装extension pack 时使用root或者sudo运行, 否则无法安装
+ ubuntu安装virutalbox最好是在官网安装, 通过ubuntu源安装的不能使用usb映射.
+ 查看git库中各个用户提交的次数,并且递减排序 `git shortlog -sn`
+ virtualbox下的windowns虚拟机需要先安装**VBoxGuestAdditions**才能成功创建共享文件夹或者共享剪贴板. `Dives->Install Guest Additions`
+ 正则表达式匹配时, 加`?`表示非贪心匹配, 直接匹配最近的符合项. eg: `<p>.*?</p>`
+ python 字符串比较, `cmp(a, b)`, 相同时返回 `0`
