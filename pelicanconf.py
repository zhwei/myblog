#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'zhwei'
SITENAME = u"zhwei"
SITESUBTITLE='记录学习，品味生活.'
SITEURL = 'http://zhangweide.cn'

TIMEZONE = 'Asia/Shanghai'

DEFAULT_LANG = u'en'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# menu items
DISPLAY_CATEGORIES_ON_MENU=False
DISPLAY_PAGES_ON_MENU=False
MENUITEMS = (
        ('Home', '/'),
        ('Archive', '/archives.html'),
        ('Notes', '/pages/notes.html'),
        ('About', '/pages/about.html'),
        )

# Blogroll
LINKS =  (
        ('42qu blog', 'http://zhwei.42qu.com/'),
        ('jekyll blog', 'http://zhwei.gitcafe.com/')
          )

# Social widget
SOCIAL = (
        ('github', 'http://github.com/zhwei'),
        ('twitter', 'http://twitter.com/zhweifcx'),
        )

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing

RELATIVE_URLS = True

# Some Settings
THEME="not-myself"
PATH_METADATA='content/img/'
DELETE_OUTPUT_DIRECTORY=True
PYGMENTS_RST_OPTIONS={'classprefix': 'pgcss', 'linenos': 'table'}
DISQUS_SITENAME = u"zhwei"
STATIC_PATHS = [u"images"]
# 生成PDF选项
#PDF_GENERATOR = True
#PDF_STYLE = 'chinese.style'
#PDF_STYLE_PATH = 'pdf'
